﻿namespace Labs
{
    public interface IBacsClient
    {
        bool Ping();
        string GetInputData();
        bool WriteAnswer<T>(T obj);
    }
}
