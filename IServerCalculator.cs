﻿namespace Labs
{
    interface IServerCalculator
    {
        string calculateAnswer(string body);
    }
}
