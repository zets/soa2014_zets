﻿namespace Labs
{
    class ServerCalculator : IServerCalculator
    {
        public string calculateAnswer(string body)
        {
            var serializer = new JsonSerialization();
            var input = serializer.deserialization<Input>(body);
            return (input == null) ? "" : serializer.serialization(new Calculator().GenerateOutput(input));
        }
    }
}
