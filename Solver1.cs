﻿using System;
using System.Collections.Generic;

namespace Labs
{
    class Solver1
    {
        public void solve()
        {
            var typeOfSerialization = Console.ReadLine();
            var inputSerialized = Console.ReadLine();

            var serialize = new Dictionary<string, ISerialization>() { {"Json", new JsonSerialization()}, {"Xml", new XmlSerialization() } };

            var input = serialize[typeOfSerialization].deserialization<Input>(inputSerialized);
            var output = new Calculator().GenerateOutput(input);
            Console.WriteLine(serialize[typeOfSerialization].serialization(output));
        }
    }
}
