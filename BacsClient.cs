﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Labs
{
  public class ClientBase
  {
    protected string urlServer;
    protected string port;

    protected HttpWebResponse getResponse(string methodName, Method method, string body = "")
    {
      //todo: заюзать WebClient, ыместо WebResponse
      var request = WebRequest.Create(string.Format("{0}:{1}/{2}", urlServer, port, methodName));
      request.Timeout = 150;
      request.ContentLength = Encoding.UTF8.GetByteCount(body);
      //todo: сделать enum Get,Post, передавать его в параметрах метода, а не ориентироваться на body
      request.Method = method == Method.Get ? "GET" : "POST";
      if (body.Length > 0)
      {
        using (var writer = request.GetRequestStream())
        {
          writer.Write(Encoding.UTF8.GetBytes(body), 0, (int)request.ContentLength);
        }
      }

      try
      {
        return (HttpWebResponse)request.GetResponse();
      }
      catch
      {
        return null;
      }
    }
  }
}
