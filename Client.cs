﻿using System.Text;
using System.Net;
using System.IO;

namespace Labs
{
    enum Method { Get, Post}
    //todo: спрять в базовый класс методы getResponse
    //todo: выделить интерфейс IBacsClient с методами WriteAnswer, Ping, GetInputData, причем работать не с сериализованными строками, а с нормальными объектами Input, Output
    class Client : ClientBase, IBacsClient
    {
        public Client(string urlServer, string port)
        {
            this.urlServer = urlServer;
            this.port = port;
        }


        //todo: здесь параметром должна быть не строка а объект Output
        public bool WriteAnswer(Output output)
        {
            var response = (HttpWebResponse)getResponse("writeAnswer", Method.Post, new JsonSerialization().Serialize<Output>(output));
            return response != null;
        }

        //todo : здесь возвращаться должен объект Input, а не тсрока
        public string GetInputData()
        {
             var response = (HttpWebResponse)getResponse("getInputData", Method.Get, new JsonSerialization().Deserialize<Input>(input););
             return new JsonSerialization().Deserialize<Input>(new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd());
        }

        public bool Ping()
        {
            var response = (HttpWebResponse)getResponse("ping", Method.Get);
            return (response != null) ? response.StatusCode == HttpStatusCode.OK : false;
        }
    }
}
